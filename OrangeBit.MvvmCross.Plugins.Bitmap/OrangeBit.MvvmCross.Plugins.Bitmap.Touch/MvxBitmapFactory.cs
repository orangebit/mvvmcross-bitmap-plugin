using System;
using System.IO;
using OrangeBit.MvvmCross.Plugins.Bitmap;
using OrangeBit.MvvmCross.Plugins.Bitmap.Touch;

namespace OrangeBit.MvvmCross.Plugins.Bitmap.Touch
{
	public class MvxBitmapFactory: IMvxBitmapFactory
	{ 
		public IMvxBitmap Create()
		{
			return new MvxTouchBitmap();
		}
	}
}