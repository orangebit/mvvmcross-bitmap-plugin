using System;
using System.Reflection;
using MonoTouch.UIKit;
using System.IO;
using MonoTouch.Foundation;

namespace OrangeBit.MvvmCross.Plugins.Bitmap.Touch
{
	public class MvxTouchBitmap : IMvxBitmap
	{
		private UIImage bitmap = null;

		public MvxTouchBitmap ()
		{	
		}

		#region IMvxBitmap implementation

		public void SetSource (byte[] bytes)
		{
			using (MemoryStream memStream = new MemoryStream ())
			{
				byte[] streamBytes = new byte [bytes.Length];
				memStream.Read( streamBytes, 0, bytes.Length);
				NSData data = NSData.FromArray( streamBytes );
				bitmap = UIImage.LoadFromData( data );
			}              
		}

		public void LoadFromResource(string assemblyName,MvxResourcePath resourcePath)
		{
			var resourceName = resourcePath.GetResourcePath (".",true);
			var strm = Assembly.Load (new AssemblyName(assemblyName)).GetManifestResourceStream(resourceName);
			NSData data = NSData.FromStream( strm );
			bitmap = UIImage.LoadFromData (data);
		}

		public int Height
		{
			get{
				return bitmap.CGImage.Height;
			}
		}

		public int Width
		{
			get{
				return bitmap.CGImage.Width;
			}
		}


		public object ToNative ()
		{
			if(bitmap == null)
				throw new NullReferenceException("You can't get native version of an empty IMvxBitmap, set a valid source and try again");

			return bitmap;
		}

		#endregion


	}
}

