using Cirrious.CrossCore;
using Cirrious.CrossCore.Plugins;

namespace OrangeBit.MvvmCross.Plugins.Bitmap.Touch
{
    public class Plugin
        : IMvxPlugin
    {
        public void Load()
        {
			Mvx.RegisterSingleton<IMvxBitmapFactory>(new MvxBitmapFactory());
        }
    }
}